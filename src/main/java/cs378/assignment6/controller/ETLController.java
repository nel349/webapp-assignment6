package cs378.assignment6.controller;

import cs378.assignment6.domain.MeetingList;
import cs378.assignment6.etl.Loader;
import cs378.assignment6.etl.Reader;
import cs378.assignment6.etl.Transformer;
import cs378.assignment6.etl.impl.BasicTransformerImpl;
import cs378.assignment6.service.EavesdropReaderService;
import cs378.assignment6.service.MeetingDataMgrService;

public class ETLController implements Runnable {

	private Reader eavesdropReader;
	private Transformer transformer;
	private Loader meetingDataMgr;
	
	public ETLController() {
		eavesdropReader = new EavesdropReaderService();
		transformer = new BasicTransformerImpl();
		meetingDataMgr = new MeetingDataMgrService();
	}
	
	private void performETLActions() {
		try {
			String source = "http://eavesdrop.openstack.org/meetings/solum_team_meeting/2014/";
			
			// 1) Read
			String data = eavesdropReader.read(source);
			
			// 2) Transform
			MeetingList transformedData = transformer.transform(data); //need to test

//			for(Meeting x : transformedData.getMeetings())
//				System.out.println("{"+ x.getName() + "}, {"+x.getDescription() + "}, {" +x.getLink()+"}, {" + x.getYear()+"}");
			// 3) Load
			meetingDataMgr.load(transformedData);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		ETLController etlController = new ETLController();
		etlController.performETLActions();
	}

	public void run() {
		performETLActions();
	}
}
