package cs378.assignment6.service;

import cs378.assignment6.domain.Meeting;
import cs378.assignment6.domain.MeetingList;
import cs378.assignment6.domain.Meeting_;
import cs378.assignment6.domain.Project;
import cs378.assignment6.etl.Loader;
import cs378.assignment6.etl.Reader;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class MeetingDataMgrService implements Loader, Reader {



	public String read(String source) throws Exception {

		return null;
	}
	private SessionFactory sessionFactory;

	public MeetingDataMgrService() {
		// A SessionFactory is set up once for an application
		sessionFactory = new Configuration()
				.configure() // configures settings from hibernate.cfg.xml
				.buildSessionFactory();
	}
	//
	public void load(MeetingList objectToLoad) throws Exception {
//		insertProject(objectToLoad);
		insertMeetings(objectToLoad);
	}
	//
	private void insertMeeting(Meeting m) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(m);
		session.getTransaction().commit();
		session.close();
	}

	private void insertMeetings(MeetingList ml){
		for(Meeting m : ml.getMeetings()){
			insertMeeting(m);
		}
	}
	//
//
//
	public Project getProjectByName(String project_name) throws Exception {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		String contextREST = "http://localhost:8080/myeavesdrop/projects/solum/meetings/";


		// Selection criteria
		List<Meeting> meetings = session.createQuery("from Meeting").list();

		Project project = new Project("solum",contextREST);

		for ( Meeting meeting : meetings ) {
			Meeting_ m = new Meeting_();
			m.setName(meeting.getName());
			m.setLink(meeting.getLink());
			m.setYear(meeting.getYear());
			m.setDescription(meeting.getDescription());
			project.add_meeting(m);
		}


		return project;
	}
}
