package cs378.assignment6.service;

/**
 * Created by norman.lopez on 11/19/14.
 */

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TransformServices {


    public List<String> getData(String link, boolean isLink){


        String url =link;
        Document doc =null;
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return this.getQueryData(doc, isLink);
    }

    public List<String> getQueryData(Document doc, boolean isLink){
        Elements links = doc.select("a[href]");
        List<String> all = new ArrayList<String>();
        int count =0;
        for (Element s_links : links) {
            String x;
            if(isLink){
                x = s_links.absUrl("href");
            }
            else{
                x = s_links.text();
            }

            if(count > 4 ){
//				String a = "<a href="+ x +">" + x +"</a>";
                all.add(x);
            }
            ++count;
        }
        return all;
    }


}
