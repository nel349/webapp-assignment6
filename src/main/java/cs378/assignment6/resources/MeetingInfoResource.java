package cs378.assignment6.resources;

import cs378.assignment6.controller.ETLController;
import cs378.assignment6.domain.Project;
import cs378.assignment6.etl.Reader;
import cs378.assignment6.service.MeetingDataMgrService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/projects")
public class MeetingInfoResource {
	
	private ETLController etlController;
	private Reader meetingDataReader;
	
	public MeetingInfoResource() {
		etlController = new ETLController();
		meetingDataReader = new MeetingDataMgrService();
		
		// Start data load
		(new Thread(etlController)).start();;
	}
	
	// Test method
	@GET
	@Path("/solum/getAll")
	public String getAll() {
		return "Hello, world";
	}
	
	@GET
	@Path("/Solum/meetings")
	@Produces("application/xml")
	public Project getMeetingList() throws Exception {

		Project ds =  meetingDataReader.getProjectByName("solum");

		return ds;
	}
}
