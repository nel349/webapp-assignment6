package cs378.assignment6.etl;

import cs378.assignment6.domain.MeetingList;

public interface Transformer {
	public MeetingList transform(String source) throws Exception;
}
