package cs378.assignment6.etl.impl;

import cs378.assignment6.domain.Meeting;
import cs378.assignment6.domain.MeetingList;
import cs378.assignment6.etl.Transformer;
import cs378.assignment6.service.TransformServices;

import java.util.List;

public class BasicTransformerImpl implements Transformer {
	String year = "2014";
	String project = "solum_team_meeting";
	String contextREST = "http://localhost:8080/myeavesdrop/projects/solum/meetings/";


	public MeetingList transform(String source) throws Exception {
		TransformServices js = new TransformServices();
		List<String> names = js.getData(source, false);
		List<String> links = js.getData(source, true);
		MeetingList result = new MeetingList();
		int i , j;
		for(i =  j = 0; i < names.size() && j < links.size() ; ++i, ++j){
			Meeting m = new Meeting(names.get(i),links.get(j), contextREST + names.get(i), year );
			result.addMeeting(m);
		}
		return result;
	}
}
