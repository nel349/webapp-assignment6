package cs378.assignment6.etl;

import cs378.assignment6.domain.MeetingList;

public interface Loader {
	public void load(MeetingList ml) throws Exception;
}
