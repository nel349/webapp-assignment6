package cs378.assignment6.etl;

import cs378.assignment6.domain.Project;

public interface Reader {
	public String read(String source) throws Exception;

	public Project getProjectByName(String source) throws Exception;
}
