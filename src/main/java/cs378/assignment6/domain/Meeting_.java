package cs378.assignment6.domain;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Meeting")
@XmlAccessorType
public class Meeting_ {


	private String name;

	private String link;

	private String year;

	private String description;

	public Meeting_(){
		name = null;
		link = null;
		year = null;
		description = null;
	}

	@XmlElement
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Meeting_(String name, String link, String year){
		this.name = name;
		this.link = link;
		this.year = year;
		this.description = year;
	}
	
	
	public void setName(String name){
		this.name = name;
	}
	@XmlElement
	public String getName(){
		return this.name;
	}
	
	public void setLink(String link){
		this.link = link;
	}
	
	@XmlElement
	public String getLink(){
		return this.link;
	}
	
	public void setYear(String year){
		this.year = year;
	}
	
	@XmlElement
	public String getYear(){
		return this.year;
	}


}
