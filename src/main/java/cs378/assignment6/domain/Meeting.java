package cs378.assignment6.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table( name = "MEETINGS" )
public class Meeting {

	private String name;
	

	private String description;

	private String year;

	private String link;
	
	public Meeting() {
	}
	
	public Meeting(String name, String description, String link, String year) {
		this.name = name;
		this.description = description;
		this.link = link;
		this.year = year;
	}
	
	@Id
	@Column(name = "name")
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	@Column(name = "description")
	public String getDescription(){
		return this.description;
	}

	public void setDescription(String description){
		this.description = description;
	}

	@Column(name = "link")
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@Column(name = "year")
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}


}